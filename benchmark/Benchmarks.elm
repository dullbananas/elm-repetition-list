module Benchmarks exposing (main)

import Benchmark exposing (..)
import Benchmark.Runner

import RepList exposing (RepList)


main : Benchmark.Runner.BenchmarkProgram
main =
    Benchmark.Runner.program suite


list : List Int
list =
    [ 2, 4, 7, 2, 2, 2, 2, 5, 6, 1, 5, 5, 5, 5, 5, 5, 5, 5, 5, 3, 7, 5, 5, 5, 3 ]


repList : RepList Int
repList =
    RepList.fromList list


comp : String -> (RepList_ -> a) -> (List_ -> b) -> Benchmark
comp name a b =
    Benchmark.compare name
        "List" (\_ -> b List_)
        "RepList" (\_ -> a RepList_)


type RepList_
    = RepList_


type List_
    = List_


suite : Benchmark
suite =
    describe "RepList"
        [ benchmark "fromList" <| \_ ->
            RepList.fromList list

        , benchmark "toList" <| \_ ->
            RepList.toList repList

        , comp "map"
            (\RepList_ ->
                RepList.map ((*) 4) repList
            )(\List_ ->
                List.map ((*) 4) list
            )

        , comp "length"
            (\RepList_ ->
                RepList.length repList
            )(\List_ ->
                List.length list
            )
        ]
