module RepList exposing
    ( RepList
    , toList, fromList
    , empty, singleton, repeat, cons
    , map, foldl, foldr, filter, filterMap
    , reverse, length, any, all
    )



-- Type --


type RepList a
    = RepList ( List { count : Int, value : a } )



-- Convert --


toList : RepList a -> List a
toList (RepList items) =
    items
        |> List.map (\{count, value} -> List.repeat count value)
        |> List.concat


fromList : List a -> RepList a
fromList =
    List.foldr cons empty



-- Create --


empty : RepList a
empty =
    RepList []


singleton : a -> RepList a
singleton =
    repeat 1


repeat : Int -> a -> RepList a
repeat count value =
    RepList [{ count = count, value = value }]


cons : a -> RepList a -> RepList a
cons newHead (RepList items) =
    RepList <| case items of
        x :: xs ->
            if x.value == newHead then
                { x | count = x.count + 1 } :: xs
            else
                { count = 1, value = newHead } :: items

        _ ->
            { count = 1, value = newHead } :: items



-- Transform --


map : (a -> b) -> RepList a -> RepList b
map func (RepList items) =
    RepList <| List.map
        (\item ->
            { count = item.count, value = func item.value }
        ) items


foldl : (a -> b -> b) -> b -> RepList a -> b
foldl func acc (RepList items) =
    foldlHelp func acc items


foldlHelp : (a -> b -> b) -> b -> List { count : Int, value : a } -> b
foldlHelp func =
    List.foldl
        ( \item acc ->
            foldlHelpHelp func acc item
        )


foldlHelpHelp : (a -> b -> b) -> b -> { count : Int, value : a } -> b
foldlHelpHelp func acc item =
    case item.count of
        0 ->
            acc

        _ ->
            foldlHelpHelp
                func
                (func item.value acc)
                { item | count = item.count - 1 }


foldr : (a -> b -> b) -> b -> RepList a -> b
foldr func acc =
    reverse >> foldl func acc


filter : (a -> Bool) -> RepList a -> RepList a
filter func (RepList items) =
    RepList <| List.filter (.value >> func) items


filterMap : (a -> Maybe b) -> RepList a -> RepList b
filterMap func (RepList items) =
    RepList <| List.foldr
        (\item acc ->
            case func item.value of
                Just newValue ->
                    { count = item.count, value = newValue } :: acc

                Nothing ->
                    acc
        ) [] items



-- Utilities --


reverse : RepList a -> RepList a
reverse (RepList items) =
    RepList <| List.reverse items


length : RepList a -> Int
length (RepList items) =
    List.foldl
        (\item acc ->
            item.count + acc
        ) 0 items


all : (a -> Bool) -> RepList a -> Bool
all func list =
    not (any (not << func) list )


any : (a -> Bool) -> RepList a -> Bool
any func (RepList items) =
    anyHelp func items


anyHelp : (a -> Bool) -> List { count : Int, value : a } -> Bool
anyHelp func items =
    case items of
        [] ->
            False

        x :: xs ->
            if func x.value then
                True
            else
                anyHelp func xs
