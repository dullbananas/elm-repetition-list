module Tests exposing (suite)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)

import RepList exposing (RepList)


suite : Test
suite =
    describe "RepList"
        [ test "singleton" <| \_ ->
            RepList.singleton 8
                |> expectList [ 8 ]

        , test "fromList" <| \_ ->
            [ 1, 2, 3, 3, 3, 4, 4, 5, 6, 7, 7 ]
                |> RepList.fromList
                |> expectList [ 1, 2, 3, 3, 3, 4, 4, 5, 6, 7, 7 ]

        , test "repeat" <| \_ ->
            RepList.repeat 32 'a'
                |> expectList (List.repeat 32 'a')

        , test "cons" <| \_ ->
            [ 4, 5, 6 ]
                |> RepList.fromList
                |> RepList.cons 4
                |> expectList [ 4, 4, 5, 6 ]

        , test "map" <| \_ ->
            [ 1, 2, 3 ]
                |> RepList.fromList
                |> RepList.map ((+) 1)
                |> expectList [ 2, 3, 4 ]

        , test "foldl" <| \_ ->
            [ 1, 2, 3, 3, 3, 4 ]
                |> RepList.fromList
                |> RepList.foldl (\a acc -> a + (a * acc)) 1
                |> Expect.equal
                    ( List.foldl (\a acc -> a + (a * acc)) 1
                        [ 1, 2, 3, 3, 3, 4 ]
                    )

        , test "foldr" <| \_ ->
            [ 1, 2, 3, 3, 3, 4 ]
                |> RepList.fromList
                |> RepList.foldr (\a acc -> a + (a * acc)) 1
                |> Expect.equal
                    ( List.foldr (\a acc -> a + (a * acc)) 1
                        [ 1, 2, 3, 3, 3, 4 ]
                    )

        , test "reverse" <| \_ ->
            [ 1, 2, 3, 3, 4 ]
                |> RepList.fromList
                |> RepList.reverse
                |> expectList [ 4, 3, 3, 2, 1 ]

        , test "filter" <| \_ ->
            [ 1, 1, 2, 3, 3, 3, 3, 5, 6, 6 ]
                |> RepList.fromList
                |> RepList.filter (modBy 2 >> (==) 0)
                |> expectList [ 2, 6, 6 ]

        , test "filterMap" <| \_ ->
            [ "1", "1", "2", "2", "a", "a", "a", "5" ]
                |> RepList.fromList
                |> RepList.filterMap String.toInt
                |> expectList [ 1, 1, 2, 2, 5 ]

        , test "length" <| \_ ->
            [ 2, 6, 6, 4, 2, 9, 9 ]
                |> RepList.fromList
                |> RepList.length
                |> Expect.equal 7

        , fuzz (list int) "any" <| \nums ->
            RepList.fromList nums
                |> RepList.any predicate
                |> Expect.equal (List.any predicate nums)

        , fuzz (list int) "all" <| \nums ->
            RepList.fromList nums
                |> RepList.all predicate
                |> Expect.equal (List.all predicate nums)
        ]


predicate : Int -> Bool
predicate n =
    n > 49


expectList : List a -> RepList a -> Expectation
expectList list repList =
    RepList.toList repList
        |> Expect.equal list
